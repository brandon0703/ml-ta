package com.example.demo.api.controller;

import com.example.demo.api.model.Feature;
import com.example.demo.api.model.ModelHasFeature;
import com.example.demo.api.model.User;
import com.example.demo.api.validation.AssignOrRemoveUserFeatureRequest;
import com.example.demo.service.FeatureService;
import com.example.demo.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/v1")
public class HomeController {
    private final UserService userService;
    private final FeatureService featureService;

    @Autowired
    public HomeController(UserService userService, FeatureService featureService) {
        this.userService = userService;
        this.featureService = featureService;
    }

    @GetMapping("/users")
    public ResponseEntity getUsers() {
        return new ResponseEntity(
                userService.getUsers()
                , HttpStatus.OK);
    }

    @PostMapping(value = "/user/store", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity addUser(@Valid @RequestBody User user) {
        User newUser = userService.addUser(user);
        return new ResponseEntity(
                newUser
                , HttpStatus.OK);
    }

    @GetMapping("/features")
    public ResponseEntity getFeatures() {
        return new ResponseEntity(
                featureService.getFeatures()
                , HttpStatus.OK);
    }

    @PostMapping(value = "/feature/store", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity addFeature(@Valid @RequestBody Feature feature) {
        Feature newFeature = featureService.addFeature(feature);
        return new ResponseEntity(
                newFeature
                , HttpStatus.OK);
    }

    @PostMapping(value = "/feature", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity assignOrRemoveUserFeature(@Valid @RequestBody AssignOrRemoveUserFeatureRequest request) {
        String jsonResponse = "";
        boolean success = false;

        String featureName = request.getFeatureName();
        String email = request.getEmail();
        boolean enable = request.isEnable();

        try {
            User user = userService.getUserByEmail(email);
            Feature feature = featureService.getFeatureByName(featureName);

            if(enable) {
                success = userService.assignFeature(user, feature);
            }
            else {
                success = userService.removeFeature(user, feature);
            }
        } catch(Exception e) {
            return new ResponseEntity(
                    e.getMessage()
                    , HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(
                jsonResponse
                , success ? HttpStatus.OK : HttpStatus.NOT_MODIFIED);
    }

    @GetMapping(value = "/feature", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity canAccess(@RequestParam(name = "email", required = false) String email, @RequestParam(name = "featureName", required = false) String featureName) {
        String jsonResponse = "";
        boolean canAccess = false;
        try {
            if ((email != null && !email.isEmpty())
                && (featureName != null && !featureName.isEmpty())
            ) {
                ModelHasFeature modelHasFeature = userService.getUserFeatureByEmailAndFeatureName(email, featureName);
                canAccess = modelHasFeature != null;
                jsonResponse = "{\"canAccess\": " + canAccess + "}";
            }

        } catch(Exception e) {
            return new ResponseEntity(
                    e.getMessage()
                    , HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(
                jsonResponse
                , HttpStatus.OK);
    }
}
