package com.example.demo.api.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "features")
public class Feature {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long feature_id;

    @Column(unique = true)
    @NotNull(message = "Feature name is required.")
    @Size(min = 1, max = 50)
    private String name;

    public Feature(){
    }

    public Feature(Long feature_id, String name){
        this.feature_id = feature_id;
        this.name = name;
    }

    public Long getId() {
        return feature_id;
    }

    public void setId(Long feature_id) {
        this.feature_id = feature_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
