package com.example.demo.api.validation;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class AssignOrRemoveUserFeatureRequest {
    @NotNull(message = "featureName is required.")
    private String featureName;
    @NotNull(message = "email is required.")
    private String email;
    @NotNull(message = "enable is required.")
    private boolean enable;

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
