package com.example.demo.api.model;

import jakarta.persistence.*;

@Entity
@Table(name = "model_has_features")
public class ModelHasFeature {

    @EmbeddedId
    private ModelHasFeatureId modelHasFeatureId;

    @ManyToOne
    @MapsId("uid")
    @JoinColumn(name = "user_id")
    User user;

    @ManyToOne
    @MapsId("fid")
    @JoinColumn(name = "feature_id")
    Feature feature;

    public ModelHasFeature(){
    }

    public ModelHasFeature(ModelHasFeatureId modelHasFeatureId){
        this.modelHasFeatureId = modelHasFeatureId;
    }

    public ModelHasFeatureId getId() {
        return modelHasFeatureId;
    }

    public void setId(ModelHasFeatureId modelHasFeatureId) {
        this.modelHasFeatureId = modelHasFeatureId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }
}
