package com.example.demo.api.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ModelHasFeatureId implements Serializable {

    @Column(name = "uid")
    private Long uid;

    @Column(name = "fid")
    private Long fid;

    public ModelHasFeatureId() {
    }

    public ModelHasFeatureId(Long uid, Long fid) {
        this.uid = uid;
        this.fid = fid;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getFid() {
        return fid;
    }

    public void setFid(Long fid) {
        this.fid = fid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelHasFeatureId that = (ModelHasFeatureId) o;
        return uid.equals(that.uid) &&
                fid.equals(that.fid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uid, fid);
    }
}
