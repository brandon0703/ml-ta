package com.example.demo.repository;

import com.example.demo.api.model.Feature;
import com.example.demo.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeatureRepository extends JpaRepository <Feature, Long> {
    Feature findByName(String name);
}
