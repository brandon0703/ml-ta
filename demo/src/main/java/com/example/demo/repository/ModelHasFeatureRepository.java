package com.example.demo.repository;

import com.example.demo.api.model.Feature;
import com.example.demo.api.model.ModelHasFeature;
import com.example.demo.api.model.ModelHasFeatureId;
import com.example.demo.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ModelHasFeatureRepository extends JpaRepository<ModelHasFeature, ModelHasFeatureId> {
    @Query("SELECT uf FROM ModelHasFeature uf " +
            "JOIN uf.user u ON u.email = :email " +
            "JOIN uf.feature f ON f.name = :featureName")
    ModelHasFeature findByUserEmailAndFeatureName(@Param("email") String email, @Param("featureName") String featureName);

    ModelHasFeature findByUserAndFeature(User user, Feature feature);
}
