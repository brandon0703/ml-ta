package com.example.demo.service;

import com.example.demo.api.model.Feature;
import com.example.demo.api.model.ModelHasFeature;
import com.example.demo.api.model.ModelHasFeatureId;
import com.example.demo.api.model.User;
import com.example.demo.helper.exception.ValidationException;
import com.example.demo.repository.ModelHasFeatureRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final ModelHasFeatureRepository mhfRepository;

    @Autowired
    public UserService(UserRepository userRepository, ModelHasFeatureRepository mhfRepository) {
        this.userRepository = userRepository;
        this.mhfRepository = mhfRepository;
    }
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUserByEmail(String email) {
        Optional<User> userOptional = Optional.ofNullable(userRepository.findByEmail(email));

        return userOptional.orElseThrow(() -> new ValidationException("User not found"));
    }

    public User addUser(User user) {
        userRepository.save(user);
        return user;
    }

    public boolean assignFeature(User user, Feature feature) {
        ModelHasFeature modelHasFeature = new ModelHasFeature();
        modelHasFeature.setUser(user);
        modelHasFeature.setFeature(feature);
        modelHasFeature.setId(new ModelHasFeatureId(user.getId(), feature.getId()));

        try {
            mhfRepository.save(modelHasFeature);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean removeFeature(User user, Feature feature) {
        ModelHasFeature modelHasFeature = mhfRepository.findByUserAndFeature(user, feature);

        if (modelHasFeature != null) {
            try {
                mhfRepository.delete(modelHasFeature);
                return true;
            } catch (Exception e) {
                return false;
            }
        }
        else{
            throw new ValidationException("Record Not Found");
        }
    }

    public ModelHasFeature getUserFeatureByEmailAndFeatureName(String email, String featureName) {
        return mhfRepository.findByUserEmailAndFeatureName(email, featureName);
    }
}
