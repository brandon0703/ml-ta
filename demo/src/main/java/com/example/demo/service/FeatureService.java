package com.example.demo.service;

import com.example.demo.api.model.Feature;
import com.example.demo.api.model.User;
import com.example.demo.helper.exception.ValidationException;
import com.example.demo.repository.FeatureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FeatureService {
    private final FeatureRepository featureRepository;

    @Autowired
    public FeatureService(FeatureRepository featureRepository) {
        this.featureRepository = featureRepository;
    }
    public List<Feature> getFeatures() {
        return featureRepository.findAll();
    }

    public Feature getFeatureByName(String name) {
        Optional<Feature> featureOptional = Optional.ofNullable(featureRepository.findByName(name));

        return featureOptional.orElseThrow(() -> new ValidationException("Feature not found"));
    }

    public Feature addFeature(Feature feature) {
        featureRepository.save(feature);

        return feature;
    }
}
